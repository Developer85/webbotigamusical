<%-- 
    Document   : registreClient
    Created on : 12-dic-2016, 16:50:16
    Author     : Alex Almirall
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Registre Client</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>


<body style="display:block; width:75%;  margin:0px auto;">


    <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a class="btn btn-info" href="index.html">Inici</a></li>
                        <li><a class="btn btn-info" href="LlistaCategoriesServlet">Catàleg</a></li>
                        <li><a class="btn btn-info" href="mailto:lostrotamusicosopinio@hotmail.com?Subject=Consulta" target="_top">Consulta'ns</a></li>                    
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a class="btn btn-info" href="registreClient.jsp">Registre</a></li>
                    </ul>
                </div>
        </nav>

    
    <header style="background: #428bca; width:75%; margin:0px auto;">
        
        <div class =" container-fluid ">
            <div class="text-center">
                <h1>Registre de nou Client</h1>
                </p>
            </div>
        </header>
    <form method="post" action="RegistrarClientServlet">
    <article style="background: #428bca; width:75%; margin:0px auto;">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="inputNom" class="col-md-4 control-label">Nom</label>
                    <div class="col-xs-6">
                        <input class="form-control" name="Nom" type="text" placeholder="Nom complet" required="true" maxlength="45">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputDNI" class="col-md-4 control-label">DNI</label>
                    <div class="col-xs-3">
                        <input  class="form-control" name="DNI" type="text" placeholder="DNI sense guions" required="true" minlength="8" maxlength="9">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPwd" class="col-md-4 control-label">Contrasenya</label>
                    <div class="col-xs-3">
                        <input  class="form-control" name="Contrasenya" type="password" placeholder="De 8 a 10 caràcters" required="true" minlength="8" maxlength="10">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputadreça" class="col-md-4 control-label">Adreça</label>
                    <div class="col-xs-6">
                        <input href class="form-control" name="Adreca" type="text" placeholder="Adreça completa" required="true" maxlength="45">
                    </div>
                </div>

                <div class="form-group">

                    <label for="inputTargeta" class="col-md-4 control-label">Targeta</label>
                    <div class="col-xs-5"> <!-- tamaño --> 
                        <input class="form-control" name="Targeta" type="text" placeholder="Número de Targeta" required="true" maxlength="20">
                    </div>
                </div>
            </div>
        </div>


        <p style="display:block; line-height: 70px; width:75%; text-align:center; margin:0 auto;">
            <input type="submit" value="Registrar" class="btn btn-primary btn-lg"></input></p>

    </article>
</form>
</body>
</html>