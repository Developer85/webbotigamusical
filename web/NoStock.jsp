<%-- 
    Document   : NoStock
    Created on : 16-dic-2016, 16:58:36
    Author     : root
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> --%>
        <title>Sense Stock</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>        
    </head>
    <body style="display:block; width:75%;  margin:0px auto;">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <ul class="nav navbar-nav navbar-left">
                    <li><a class="btn btn-info" href="index.html">Inici</a></li>
                    <li class="active"><a class="btn btn-info" href="LlistaCategoriesServlet">Catàleg</a></li>
                    <li><a class="btn btn-info" href="mailto:lostrotamusicosopinio@hotmail.com?Subject=Consulta" target="_top">Consulta'ns</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a class="btn btn-info" href="registreClient.jsp">Registre</a></li>
                </ul>
            </div>
        </nav>
        
        <br><br>
        <h1 style="width:75%; margin:0px auto; text-align: center">Sense Stock</h1>
        
        <br><br>
        <p style="display:block; line-height: 70px; width:100%; text-align:center; margin:0 auto;">
            <a class="btn btn-primary btn-lg" href="index.html" role="button">Tornar</a></p>
    </body>
</html>
