<%-- 
    Document   : VerificarCompra
    Created on : 14-dic-2016, 16:23:30
    Author     : root
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Confirmacio compra</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>

    <body style="display:block; width:75%;  margin:0px auto;">
        <nav class="navbar navbar-default  ">
            <div class="container-fluid">
                <ul class="nav navbar-nav navbar-left">
                    <li><a class="btn btn-info" href="index.html">inici</a></li>
                    <li class="active"><a class="btn btn-info" href="LlistaCategoriesServlet">Catàleg</a></li>
                    <li><a class="btn btn-info" href="mailto:lostrotamusicosopinio@hotmail.com?Subject=Consulta" target="_top">Consulta'ns</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a class="btn btn-info" href="registreClient.jsp">Registre</a></li>
                </ul>
            </div>
        </nav>

        <article style="text-align: center; background: #428bca">
            <form method="post" action="ValidarVendaServlet?codi=${article.numSerie}">
                <br>
                <h1>Compra de ${article.nomInstrument} (${article.numSerie})</h1>
                <h2>Preu: ${article.preu} €</h2>
                <br>
                <h3>Només els clients registrats poden comprar instruments.</h3>
                <br>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="inputDni" class="col-md-5 control-label">Dni</label>
                        <div class="col-xs-2">
                            <input  class="form-control" name="Dni" type="text" placeholder="Posa el DNI">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPass" class="col-md-5 control-label">Contrasenya</label>
                        <div class="col-xs-2">
                            <input class="form-control" name="Pass" type="password" placeholder="Posa la contrasenya">
                        </div>
                    </div>
                </div>

                <h3>Es carregarà l'import de ${article.preu} € a la seva targeta.</h3>

                <p style="display:block; line-height: 70px; width:100%; text-align:center; margin:0 auto;">
                    <input type="submit" class="btn btn-info" value="Confirmar Venda">
            </form>
        </article>
    </body>
</html>
