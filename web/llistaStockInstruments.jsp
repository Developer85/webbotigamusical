<%-- 
    Document   : llistaStockInstruments
    Created on : Dec 11, 2016, 1:36:39 PM
    Author     : usuari
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> --%>
        <title>Stock Disponible</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>        
    </head>
    <body style="display:block; width:75%;  margin:0px auto;">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <ul class="nav navbar-nav navbar-left">
                    <li><a class="btn btn-info" href="index.html">Inici</a></li>
                    <li class="active"><a class="btn btn-info" href="LlistaCategoriesServlet">Catàleg</a></li>
                    <li><a class="btn btn-info" href="mailto:lostrotamusicosopinio@hotmail.com?Subject=Consulta" target="_top">Consulta'ns</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a class="btn btn-info" href="registreClient.jsp">Registre</a></li>
                </ul>
            </div>
        </nav>
        
        <h1>Stock Disponible</h1>
        <br><br>
        
        <table class="table table-stripped">
            <thead>
                <tr>
                    <th>Núm. de Sèrie</th>
                    <th>Nom d'Instrument</th>
                    <th>Estat</th>
                    <th>Preu (€)</th>
                    <th</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${articles}" var="art">
                    <tr>
                        <td>${art.numSerie}</td>
                        <td>${art.nomInstrument}</td>
                        <td>${art.estat}</td>
                        <td>${art.preu}</td>
                        <td><a class="btn btn-info" href="ConfirmarVendaServlet?id=${art.numSerie}">Comprar</a></td>                        
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </body>
</html>
