<%-- 
    Document   : Factura
    Created on : 08-dic-2016, 20:13:08
    Author     : Juan je
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Factura</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>


<body style="display:block; width:75%;  margin:0px auto;">


    <nav class="navbar navbar-default  ">
        <div class="container-fluid">
            <!--<div class="navbar-header">
              <a class="navbar-brand" href="#">Botiga Musical</a>
            </div>-->
            <ul class="nav navbar-nav navbar-left">
                <li class="active"><a class="btn btn-info" href="#">Home</a></li>
                <li><a class="btn btn-info" href="LlistaCategoriesServlet">Catàleg</a></li>
                <li><a class="btn btn-info" href="mailto:lostrotamusicosopinio@hotmail.com?Subject=Consulta" target="_top">Consulta'ns</a></li>
            </ul>
        </div>
    </nav>

    
    <header style="background: #428bca">
        
        <div class =" container-fluid ">
            <div class="text-center">
                <h1>Factura</h1>
                </p>
            </div>
        </header>
    
    <article style="background: #428bca">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="inputNom" class="col-md-4 control-label">Nom del Comprador</label>
                    <div class="col-xs-5">
                        <input class="form-control" id="Nombre" type="text" value="${client.nom_client}" disabled>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputIntrument" class="col-md-4 control-label">Instrument</label>
                    <div class="col-xs-5">
                        <div class="form-inline">

                            <input class="form-control" id="NomInstrument" type="text" value="${article.nomInstrument}" disabled>
                            <input class="form-control" id="NumInstrument" type="text" value="${article.numSerie}" disabled>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPreu" class="col-md-4 control-label">Preu de Venda</label>
                    <div class="col-xs-2">
                        <input class="form-control" id="Preu" type="text" value="${article.preu} €." disabled>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputadreça" class="col-md-4 control-label">Adreça d'Enviament</label>
                    <div class="col-xs-5">
                        <input href class="form-control" id="Adreça" type="text" value="${client.direccio_env}" disabled>
                    </div>
                </div>

                <div class="form-group">

                    <label for="inputTargeta" class="col-md-4 control-label">Targeta Bancària</label>
                    <div class="col-xs-4"> <!-- tamaño --> 
                        <input class="form-control" id="Targeta" type="text" value="${client.num_targeta}" disabled>
                    </div>
                </div>
            </div>
        

        <p style="display:block; line-height: 70px; width:100%; text-align:center; margin:0 auto;">
            <a class="btn btn-primary btn-lg" href="index.html" role="button">Tornar</a></p>

    </article>
</body>
</html>
