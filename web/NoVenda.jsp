<%-- 
    Document   : NoVenda
    Created on : Dec 16, 2016, 11:54:21 AM
    Author     : usuari
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
      <head>
        <title>Error de Confirmació</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
    <nav class="navbar navbar-default" style="width:75%; margin:0px auto;">
            <div >
                <div class="container-fluid">
                    <ul class="nav navbar-nav navbar-left">
                        <li class="active"><a class="btn btn-info" href="index.html">Inici</a></li>
                        <li><a class="btn btn-info" href="LlistaCategoriesServlet">Catàleg</a></li>
                        <li><a class="btn btn-info" href="mailto:lostrotamusicosopinio@hotmail.com?Subject=Consulta" target="_top">Consulta'ns</a></li>                     
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="btn btn-info" href="registreClient.jsp">Registre</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    <br>
        <h1 style="width:75%; margin:0px auto; text-align: center">El nom d'usuari o la contrasenya són incorrectes</h1><br>
        
        <p style="display:block; line-height: 70px; width:100%; text-align:center; margin:0 auto;">
            <a class="btn btn-primary btn-lg" href="index.html" role="button">Tornar</a></p>
        
        
    </body>
</html>
