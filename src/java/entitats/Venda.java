/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitats;

/**
 *
 * @author root
 */
public class Venda {
    
    private int idvenda;
    private String num_serie;
    private String dni;
    private float preu;

    /**
     * @return the idvenda
     */
    public int getIdvenda() {
        return idvenda;
    }

    /**
     * @return the num_serie
     */
    public String getNum_serie() {
        return num_serie;
    }

    /**
     * @return the dni
     */
    public String getDni() {
        return dni;
    }

    /**
     * @return the preu
     */
    public float getPreu() {
        return preu;
    }
    
    public Venda (String num_serie, String dni , float preu)
    {
    this.num_serie = num_serie;
    this.dni = dni;
    this.preu = preu;
    
    }
    
    
    
}
