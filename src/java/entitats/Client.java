/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitats;

/**
 *
 * @author root
 */
public class Client {
    private String dni;
    private String nom_client;
    private String num_targeta;
    private String contrasenya;
    private String direccio_env;

    /**
     * @return the dni
     */
    public String getDni() {
        return dni;
    }

    /**
     * @return the nom_client
     */
    public String getNom_client() {
        return nom_client;
    }

    /**
     * @return the num_targeta
     */
    public String getNum_targeta() {
        return num_targeta;
    }
    
    /**
     * 
     * @param num_targeta 
     */
    public void setNum_targeta(String num_targeta) {
        this.num_targeta = num_targeta;
    }

    /**
     * @return the contrasenya
     */
    public String getContrasenya() {
        return contrasenya;
    }

    /**
     * @return the direccio_env
     */
    public String getDireccio_env() {
        return direccio_env;
    }

    public Client(String dni, String nom_client, String num_targeta, String contrasenya, String direccio_env) {
        this.dni = dni;
        this.nom_client = nom_client;
        this.num_targeta = num_targeta;
        this.contrasenya = contrasenya;
        this.direccio_env = direccio_env;
    }

    @Override
    public String toString() {
        return "dni=" + dni + ", nom_client=" + nom_client + ", num_targeta=" + num_targeta + ", contrasenya=" + contrasenya + ", direccio_env=" + direccio_env;
    }
}
