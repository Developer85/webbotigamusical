/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitats;

/**
 *
 * @author root
 */
public class Factura {

    private String nom_client;
    private String nomInstrument;
    private String num_serie;
    private float preu;
    private String direccio_env;
    private String num_targeta;

    /**
     * @return the nom_client
     */
    public String getNom_client() {
        return nom_client;
    }

    /**
     * @return the nomInstrument
     */
    public String getNomInstrument() {
        return nomInstrument;
    }

    /**
     * @return the num_serie
     */
    public String getNum_serie() {
        return num_serie;
    }

    /**
     * @return the preu
     */
    public float getPreu() {
        return preu;
    }

    /**
     * @return the direccio_env
     */
    public String getDireccio_env() {
        return direccio_env;
    }

    /**
     * @return the num_targeta
     */
    public String getNum_targeta() {
        return num_targeta;
    }

    public Factura(String nomClient, String nomInstrument, String numSerie,
            float preu, String direccionEnv, String numTargeta) {

        this.nom_client = nomClient;
        this.nomInstrument = nomInstrument;
        this.num_serie = numSerie;
        this.preu = preu;
        this.direccio_env = direccionEnv;
        this.num_targeta = numTargeta;
                
    }

}
