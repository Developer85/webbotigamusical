
package entitats;

/**
 *
 * @author root
 */
public class Article {
    private int idArticle;
    private String numSerie;
    private String estat;
    private String nomInstrument;
    private String tipusInstrument;
    private float preu;

    public Article(String nomInstrument) {
        this.nomInstrument = nomInstrument;
        numSerie = estat = tipusInstrument = "";
        preu = 0f;
    }
    
    public Article(String numSerie, String estat, String nomInstrument, String tipusInstrument, float preu) {
        this.numSerie = numSerie;
        this.estat = estat;
        this.nomInstrument = nomInstrument;
        this.tipusInstrument = tipusInstrument;
        this.preu = preu;
    }

    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }
    
    public String getNumSerie() {
        return numSerie;
    }
    
    public String getEstat() {
        return estat;
    }

    public String getNomInstrument() {
        return nomInstrument;
    }

    public String getTipusInstrument() {
        return tipusInstrument;
    }

    public float getPreu() {
        return preu;
    }
    
    @Override
    public String toString() {
        return "idArticle=" + idArticle + " numSerie=" + numSerie + " estat=" + estat +
                " nomInstrument=" + nomInstrument + " tipusInstrument=" + tipusInstrument +
                " preu=" + preu;
    }
}
