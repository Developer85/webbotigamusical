
package logicaNegoci;

import entitats.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author root
 */
public class AdminBotiga {
    public ArrayList getCategories() {
        ConnectorDB cdb = new ConnectorDB();
        ArrayList<Article> llista = new ArrayList<>();
        
        try {
            cdb.conectar();
            
            Statement st = cdb.getConn().createStatement();
            String sql = "select distinct nom_instrument FROM stock";
            ResultSet rs = st.executeQuery(sql);
            
            while (rs.next()) {
                Article art = new Article(rs.getString("nom_instrument"));
                llista.add(art);
            }
        } catch (SQLException ex) {
            System.out.println("Error al conectar a la BB.DD.");
        } finally {
            cdb.desconectar();
        }
        
        return llista;
    }
    
    public ArrayList getStockDisponible(String instrument) {
        ConnectorDB cdb = new ConnectorDB();
        ArrayList<Article> llista = new ArrayList<>();
        
        try {
            cdb.conectar();
            
            Statement st = cdb.getConn().createStatement();
            String sql = "select * from stock where nom_instrument like '" + instrument + "'";
            ResultSet rs = st.executeQuery(sql);
            
            while (rs.next()) {
                Article art = new Article(rs.getString("num_serie"), rs.getString("desc_estat"),
                        rs.getString("nom_instrument"), rs.getString("tipus_instrument"),
                        rs.getFloat("preu"));
                art.setIdArticle(rs.getInt("idstock"));
                llista.add(art);
            }
        } catch (SQLException ex) {
            System.out.println("Error al conectar a la BB.DD.");
        } finally {
            cdb.desconectar();
        }
        
        return llista;
    }
    
    public Article getArticle(String numSerie) {
        ConnectorDB cdb = new ConnectorDB();
        Article art = null;
        
        try {
            cdb.conectar();
            
            Statement st = cdb.getConn().createStatement();
            String sql = "select * from stock where num_serie like '" + numSerie + "'";
            ResultSet rs = st.executeQuery(sql);
            
            while (rs.next()) {
                art = new Article(rs.getString("num_serie"), rs.getString("desc_estat"),
                        rs.getString("nom_instrument"), rs.getString("tipus_instrument"),
                        rs.getFloat("preu"));
                art.setIdArticle(rs.getInt("idstock"));
            }
        } catch (SQLException ex) {
            System.out.println("Error al conectar a la BB.DD.");
        } finally {
            cdb.desconectar();
        }
        
        return art;
    }
    
    public void deleteArticle(String numSerie) {
        ConnectorDB cdb = new ConnectorDB();
        
        try {            
            cdb.conectar();
            
            Statement st = cdb.getConn().createStatement();
            String sql = "delete from stock where num_serie like '" + numSerie + "'";
            st.execute(sql);
            //System.out.println("El artículo se ha eliminado correctamente.");
        } catch (SQLException ex) {
            //System.out.println("Error al conectar a la BB.DD.");
            ex.printStackTrace();
        } finally {
            cdb.desconectar();
        }
    }
    
    public boolean existsClient(String dni, String contrasenya) {
        ConnectorDB cdb = new ConnectorDB();
        boolean existe = false;
        
        try {            
            cdb.conectar();
            
            Statement st = cdb.getConn().createStatement();
            String sql = "select * from clients where dni like '" +
                    dni + "' and contrasenya like '" + contrasenya + "'";
            ResultSet rs = st.executeQuery(sql);            
                        
            if (rs.next()) {
                existe = true;
            }
        } catch (SQLException ex) {
            System.out.println("Error al conectar a la BB.DD.");
        } finally {
            cdb.desconectar();
        }
        
        return existe;
    }
        
    public boolean existsStock() {
        ConnectorDB cdb = new ConnectorDB();
        boolean existe = false;
        
        try {            
            cdb.conectar();
            
            Statement st = cdb.getConn().createStatement();
            String sql = "select * from stock";
            ResultSet rs = st.executeQuery(sql);            
                        
            if (rs.next()) {
                existe = true;
            }
        } catch (SQLException ex) {
            System.out.println("Error al conectar a la BB.DD.");
        } finally {
            cdb.desconectar();
        }
        
        return existe;
    }
    
    public void insertClient(Client cli) {
        ConnectorDB cdb = new ConnectorDB();
        
        try {            
            cdb.conectar();
            
            Statement st = cdb.getConn().createStatement();
            String sql = "insert into clients (dni, nom_client, num_targeta, contrasenya, direccio_env) values ('" +
                    cli.getDni() + "', '" + cli.getNom_client() + "', '" + cli.getNum_targeta() +
                    "', '" + cli.getContrasenya() + "', '" + cli.getDireccio_env() + "')";
                    
            st.execute(sql);
            //System.out.println("El cliente se ha insertado correctamente.");
        } catch (SQLException ex) {
            System.out.println("Error al conectar a la BB.DD.");
        } finally {
            cdb.desconectar();
        }
    }
    
    public Client getClient(String dni, String contrasenya) {
        ConnectorDB cdb = new ConnectorDB();
        Client c = null;
        
        try {
            cdb.conectar();
            
            Statement st = cdb.getConn().createStatement();
            String sql = "select * from clients where dni like '" +
                    dni + "' and contrasenya like '" + contrasenya + "'";
            ResultSet rs = st.executeQuery(sql);
            
            while (rs.next()) {
                c = new Client(rs.getString("dni"), rs.getString("nom_client"),
                        rs.getString("num_targeta"), rs.getString("contrasenya"), rs.getString("direccio_env"));
            }
        } catch (SQLException ex) {
            System.out.println("Error al conectar a la BB.DD.");
        } finally {
            cdb.desconectar();
        }
        
        return c;
    }
    
    public void insertVenda(String dni, String numSerie, float preu) {
        ConnectorDB cdb = new ConnectorDB();
        
        try {            
            cdb.conectar();
            
            Statement st = cdb.getConn().createStatement();
            String sql = "insert into vendes (num_serie, dni, preu) values ('" +
                    numSerie + "', '" + dni + "', " + preu + ")";
                    
            st.execute(sql);
            //System.out.println("El cliente se ha insertado correctamente.");
        } catch (SQLException ex) {
            System.out.println("Error al conectar a la BB.DD.");
        } finally {
            cdb.desconectar();
        }
    }
    
    public String obfuscateNumTargeta(String numTargeta) {
        String nouNum = "";
        for (int i=0; i<numTargeta.length()-3; i++) {
            nouNum += "*";
        }
        return nouNum + numTargeta.substring(numTargeta.length()-3);
    }
}
