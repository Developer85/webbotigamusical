
package logicaNegoci;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author root
 */
public class ConnectorDB {
    private Connection conn;
    private String db;
    private String user;
    private String pass;
    
    public ConnectorDB() {
        conn = null;
        db = "jdbc:mysql://localhost/botigamusical";
        user = "root";
        pass = "lenovo1";        
    }

    public Connection getConn() {
        return conn;
    }
    
    public void conectar() {
        conn = null;
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(db, user, pass);
        } catch (ClassNotFoundException ex) {
            System.out.println("Error al cargar el driver.");
        } catch (SQLException ex) {
            System.out.println("Error al conectar a la BB.DD.");
        }
    }
    
    public void desconectar() {
         try {
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error al desconectar de la BB.DD.");
        }
    }
}
