/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import entitats.Article;
import entitats.Client;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import logicaNegoci.AdminBotiga;

/**
 *
 * @author root
 */
public class ValidarVendaServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        AdminBotiga admBotiga = new AdminBotiga();
        String dni = request.getParameter("Dni");
        String contrasenya = request.getParameter("Pass");
        String numSerie = request.getParameter("codi");
        Article art = admBotiga.getArticle(numSerie);
        //System.out.println(art);
        
        RequestDispatcher rd = null;
        
        if (admBotiga.existsClient(dni, contrasenya)) {
            Client cli = admBotiga.getClient(dni, contrasenya);
            cli.setNum_targeta(admBotiga.obfuscateNumTargeta(cli.getNum_targeta()));
            //System.out.println(cli);
            
            admBotiga.deleteArticle(numSerie);
            //System.out.println("Article ELIMINAT!!!");
            admBotiga.insertVenda(dni, numSerie, art.getPreu());
            //System.out.println("Venda EFECTUADA!!!");
            request.setAttribute("article", art);
            request.setAttribute("client", cli);
            rd = request.getRequestDispatcher("Factura.jsp");
        } else {
            rd = request.getRequestDispatcher("NoVenda.jsp");
        }

        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
