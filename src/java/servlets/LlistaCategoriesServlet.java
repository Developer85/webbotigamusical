
package servlets;

import entitats.Article;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import logicaNegoci.AdminBotiga;

/**
 *
 * @author root
 */
public class LlistaCategoriesServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        RequestDispatcher rd = null;
        AdminBotiga admBotiga = new AdminBotiga();
        
        if (admBotiga.existsStock()) {
            //System.out.println("HAY STOCK");
            ArrayList<Article> llista = admBotiga.getCategories();
            request.setAttribute("articles", llista);
            rd = request.getRequestDispatcher("llistaCategInstruments.jsp");
        } else {
            //System.out.println("NO HAY STOCK");
            rd = request.getRequestDispatcher("NoStock.jsp");
        }
            
        
//        ArrayList<Article> llista = new ArrayList<>();
//        AdminBotiga admBotiga = new AdminBotiga();
//
//        llista = admBotiga.getCategories();
//        request.setAttribute("articles", llista);
//        
//        RequestDispatcher rd = request.getRequestDispatcher("llistaCategInstruments.jsp");
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
